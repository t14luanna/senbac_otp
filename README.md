# Staff Admin

## Dependencies

Install dependencies with the command:

```shell
yarn install
```

## Environment setup

Default PORT is `3000`
PORT can be change by creating a file named `.env.development` with content:

```shell
PORT=3000
```

Ref: [How to use `.env` files  properly](https://facebook.github.io/create-react-app/docs/adding-custom-environment-variables#what-other-env-files-can-be-used)

## Development - How to run

Start the dev-server using the command:

```shell
yarn start
```

## Deployment

Deployment will be handled by CI-CD.

The `Dockerfile*` is used for deployment-only.
