import React, { Component } from 'react';
import uuid from 'uuid';
import { Link, withRouter } from 'react-router-dom';
import './SideBar.scss';
import { getProfile } from '../../services/AuthService';

const Admin = 'Admin';

const staffLinks = [
  {
    pathname: '/',
    name: 'Users',
    icon: 'icon-supervisor-account',
    acceptedRoles: [Admin],
  },
  {
    pathname: '/parameters',
    name: 'Parameters',
    icon: 'icon-supervisor-account',
    acceptedRoles: [Admin],
  },
  {
    pathname: '/cdrs',
    name: 'Cdr',
    icon: 'icon-supervisor-account',
    acceptedRoles: [Admin],
  },
];

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userProfile: getProfile(),
    };
  }

  handleSidebarClose = (e) => {
    document.getElementById('mySidebar').classList.remove('is-open');
    document.getElementById('overlay').classList.remove('is-open');
  };


  render() {
    const { location } = this.props;
    const { userProfile } = this.state;

    const lists = staffLinks;
    const currentStaffRole = userProfile.userType;
    return (
      <nav
        className="navbar-default animate-left fgs__sidebar"
        id="mySidebar"
      >
        <div className="sidebar-collapse">
          <ul className="nav metismenu fgs__sidebar__menu" id="side-menu">
            <li className="fgs__sidebar__header">
              <button
                className="button-collapse"
                onClick={this.handleSidebarClose}
              >
                <i className="icon-close" />
              </button>
            </li>
            {
              lists.filter(item => item.acceptedRoles.includes(currentStaffRole)).map((item) => {
                return (
                  <li
                    key={uuid.v4()}
                    className={`fgs__menu__item ${location.pathname === item.pathname && 'active'}`}
                  >
                    <Link to={item.pathname} onClick={this.handleSidebarClose}>
                      <i className={item.icon} />
                      <span className="nav-label">{item.name}</span>
                    </Link>
                  </li>
                );
              })
            }
          </ul>
        </div>
      </nav>
    );
  }
}

export default withRouter(SideBar);
