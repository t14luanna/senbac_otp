import React, { Component } from 'react';
import {
  DropdownButton,
  MenuItem,
} from 'react-bootstrap';
import { getGreetingTime } from '../../utils/utils';

import './Header.scss';
// import profile from '../../images/profile_small.jpg';
import { getProfile, logout } from '../../services/AuthService';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userProfile: getProfile(),
    };
  }

  onClickLogout = () => {
    logout();
    this.props.history.push('login');
  }

  handleSidebarOpen = () => {
    document.getElementById('mySidebar').classList.add('is-open');
    document.getElementById('overlay').classList.add('is-open');
  };

  copyToClipboard = (str) => {
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  };


  render() {
    const { userProfile } = this.state;

    return (
      <div className="header fgs__header">
        <nav className="navbar navbar-static-top fgs__header__container">
          <button className="header__menu fgs__btn" onClick={this.handleSidebarOpen}>
            <i className="icon-menu" />
          </button>

          <div className="greeting">{getGreetingTime(Date.now())}, {userProfile.username || ''}!</div>

          <DropdownButton
            id="dropdownMenu"
            className="dropdown__toggle__profile"
            title={
              <div className="dropdown__profile__info">
                <p className="dropdown__profile__name">{`${userProfile.username}`}</p>
                <p className="dropdown__profile__role">{userProfile.displayRole}</p>
              </div>
            }
          >
            <MenuItem eventKey="1" onClick={this.onClickLogout} className="dropdown__menu__item">
              Logout
            </MenuItem>
          </DropdownButton>
        </nav>

      </div>
    );
  }
}

export default Header;
