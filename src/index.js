import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import './styles/lib/animate.css';
import './styles/lib/inspinia.css';
import './styles/index.scss';

import App from './App';
import Admin from './routes/Admin';
import Page404 from './pages/Page404/Page404';
import Login from './pages/Admin/Login/Login';

ReactDOM.render(
  <BrowserRouter>
    <div>
      <Route
        path="/"
        render={() => (
          <App>
            <Switch>
              <Route path="/login" component={Login} />
              <Route path="/" component={Admin} />
              <Route component={Page404} />
            </Switch>
          </App>
        )}
      />
    </div>
  </BrowserRouter>,
  document.getElementById('root'),
);
