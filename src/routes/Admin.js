import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import * as authService from '../services/AuthService';

import Page404 from '../pages/Page404/Page404';
import Parameters from '../pages/Admin/Parameters';

import User from '../pages/Admin/Users/User';

const ADMIN = 'Admin';

export const AdminRoute = ({
  component: Component,
  path,
  authorized,
  isUser,
  acceptedRoles,
  ...rest
}) => {
  if (isUser) {
    if (acceptedRoles.includes(isUser.userType)) {
      return (
        <AuthorizedRoute
          component={Component}
          path={path}
          isUser={isUser}
          {...rest}
        />
      );
    }
    return (
      <Redirect
        to={{
          pathname: '/',
        }}
      />
    );
  }
  return (
    <UnauthorizedRoute
      component={Component}
      path={path}
      {...rest}
    />
  );
};

export const AuthorizedRoute = ({ component: Component, isUser, ...rest }) => {
  return (<Route
    {...rest}
    render={(props) => {
      return isUser ? (
        <Component {...props} {...rest} />
      ) : (
        <Redirect
          to={{
            pathname: '/login',
          }}
        />
        );
    }}
  />);
};

export const UnauthorizedRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => <Component {...props} {...rest} />} />
);

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
    };
  }

  async componentDidMount() {  //eslint-disable-line
    const { history } = this.props;
    authService.loggedIn().then(() => {
      try {
        this.setState({ user: authService.getProfile() });
      } catch (err) {
        authService.logout();
        history.push('login');
      }
    }, () => history.push('login'));
  }

  render() {
    const isUser = this.state.user;
    if (!isUser) {
      return <div>Loading</div>;
    }

    return (
      <Switch>
        <AdminRoute
          exact
          path="/"
          component={User}
          authorized
          isUser={isUser}
          acceptedRoles={[ADMIN]}
        />
        <AdminRoute
          path="/parameters"
          component={Parameters}
          authorized
          isUser={isUser}
          acceptedRoles={[ADMIN]}
        />
        <AdminRoute
          path="/cdr"
          component={Parameters}
          authorized
          isUser={isUser}
          acceptedRoles={[ADMIN]}
        />
        <Route component={Page404} />
      </Switch>
    );
  }
}

export default Admin;
