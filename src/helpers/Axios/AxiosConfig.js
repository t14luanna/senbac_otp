import axios from 'axios';
import config from '../../config';

const API_URL = config.API_HOST;

const API = axios.create({
  baseURL: API_URL,
});

API.defaults.headers.post['Content-Type'] = 'application/json';

export default API;

