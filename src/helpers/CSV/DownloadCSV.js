export default (data, filename) => {
  // Create an a tag, attached useful property to download
  const downloadElement = document.createElement('a');
  downloadElement.href = `data:text/csv;charset=utf-8,%EF%BB%BF ${encodeURIComponent(data)}`;
  downloadElement.target = '_blank';
  downloadElement.download = `${filename}.csv`;
  // Fire a download
  downloadElement.click();
};
