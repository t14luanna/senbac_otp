import React, { Component } from 'react';
import { Form } from 'react-bootstrap';
import * as authService from '../../../services/AuthService';
import AuthenticationCard from '../../../components/AuthenticationCard';
import LoadingSpinner from '../../../components/LoadingSpinner/LoadingSpinner';

import './Login.scss';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isSubmited: false,
      error: null,
      isLoading: false,
      isLogined: false,
    };
  }

  async componentDidMount() { //eslint-disable-line
    authService.loggedIn().then(() => this.props.history.push('/')).catch(err => console.log(err));
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
    if (e.target.name === 'username') {
      this.setState({
        error: null,
      });
    }
  }

  login = (e) => {
    e.preventDefault();
    this.setState({
      isSubmited: true,
      isLoading: true,
    });
    authService.login(this.state.username, this.state.password)
      .then(() => this.props.history.push('/'))
      .catch(({ err }) => {
        this.setState({ isLoading: false });
        if (err && err.username) {
          this.setState({ error: err.username });
        }
      });
  }

  render() {
    const {
      username,
      password,
      isSubmited,
      error,
      isLoading,
      isLogined,
    } = this.state;
    return (
      <div className={`page page__login ${isLogined ? 'logined' : ''}`}>
        <AuthenticationCard title="Welcome to Finger Scan System">
          <Form className="fgs__form" onSubmit={this.login}>
            <div className="fgs__form__group">
              <label className="fgs__box__label m__b--8">Email</label>
              <div className={`fgs__input__container ${((!username && isSubmited) || error) ? 'err' : ''}`}>
                <input
                  type="username"
                  placeholder="Email"
                  onChange={this.handleChange}
                  name="username"
                  value={username}
                  className="fgs__input__control"
                  required
                />
                <i className="icon-info" />
              </div>
            </div>
            <div className="fgs__form__group">
              <label className="fgs__box__label m__b--8">Password</label>
              <div className={`fgs__input__container ${((!password && isSubmited) || error) ? 'err' : ''}`}>
                <input
                  type="password"
                  placeholder="Password"
                  onChange={this.handleChange}
                  name="password"
                  value={password}
                  className="fgs__input__control"
                  required
                />
                <i className="icon-info" />
              </div>
            </div>
            <div className="text-center">
              <p className="text--error">{error}</p>
              <button
                type="submit"
                disabled={isLoading}
                className="fgs__btn bg--secondary-aquamarine fgs__btn--large min__w--174 m__t--40"
              >
                {isLoading ? <LoadingSpinner isLoading={isLoading} size={10} /> : 'Login'}
              </button>
              <p className="login__form__forgotpass" />
            </div>
          </Form>
        </AuthenticationCard>
      </div>
    );
  }
}

export default Login;
