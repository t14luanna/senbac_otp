import React, { Component } from 'react';
import { FormGroup, InputGroup } from 'react-bootstrap';
import { DebounceInput } from 'react-debounce-input';

import CreateUserModal from '../../../components/Modal/CreateUserModal';
import StatusModal from '../../../components/Modal/StatusModal/StatusModal';
import UpdateUserModal from '../../../components/Modal/UpdateUserModal/UpdateUserModal';
import Header from '../../../layout/Header/Header';
import SideBar from '../../../layout/SideBar/SideBar';
import UserTable from './UserTable/UserTable';
import SuccessModal from '../../../components/Modal/SuccessModal/SuccessModal';
import InfoModal from '../../../components/Modal/InfoModal/InfoModal';

import { defaultPageSize } from '../../../const/const';
import { API } from '../../../helpers';
import LoadingSpinner from '../../../components/LoadingSpinner/LoadingSpinner';
import { getProfile } from '../../../services/AuthService';

import './User.scss';
import WarningModal from '../../../components/Modal/WarningModal/WarningModal';
import config from '../../../config';

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: getProfile(),
      showCreateUserModal: false,
      showStatusModal: false,
      showUpdateUserModal: false,
      data: [],
      currentPage: 1,
      perPage: defaultPageSize,
      totalItems: 0,
      keyword: '',
      user: null,
      isLoading: false,
      infoMessage: '',
      isErrInfo: false,
      showWarningModal: false,
      warningMessage: '',
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  onSearch = (e) => {
    this.setState({ keyword: e.target.value, currentPage: 1 }, () => {
      this.fetchData(this.state.keyword);
    });
  };

  onChangePage = (page) => {
    this.setState({ currentPage: page }, () => {
      this.fetchData(this.state.keyword);
    });
  };

  onChangePerPage = (size) => {
    this.setState({ perPage: size, currentPage: 1 }, () => {
      this.fetchData(this.state.keyword);
    });
  };

  fetchData = (
    keyword = this.state.keyword,
    perPage = this.state.perPage,
    page = this.state.currentPage,
  ) => {
    this.setState({ isLoading: true });
    API.post(config.routes.user.query, {
      username: keyword,
      fullName: keyword,
      email: keyword,
      searchCondition: keyword,
      limit: perPage,
      page,
    }).then((res) => {
      const { data } = res.data;
      const users = data.body || [];
      const items = users.map((v) => {
        return {
          id: v.id,
          username: v.username,
          aes: {
            keyAES: v.keyAES,
            ivAES: v.ivAES,
          },
          info: {
            email: v.email,
            fullName: v.fullName,
          },
          status: {
            activated: v.activated,
            language: v.language,
            createdAt: v.createdAt,
            updatedAt: v.updatedAt,
          },
          note: {
            note: v.note,
            trunks: v.trunks,
          },
        };
      });

      this.setState({
        totalItems: data.total_count,
        data: items,
        isLoading: false,
      });
    });
  };

  closeCreateUserModal = () => {
    this.setState({ showCreateUserModal: false });
  };

  openCreateUserModal = () => {
    this.setState({ showCreateUserModal: true });
  };

  openStatusModal = () => {
    this.setState({ showStatusModal: true });
  };

  closeStatusModal = () => {
    this.setState({ showStatusModal: false });
  };

  openUpdateUserModal = (rowInfo) => {
    this.setState({ showUpdateUserModal: true, user: rowInfo });
  }

  closeUpdateUserModal = () => {
    this.setState({ showUpdateUserModal: false });
  };

  toggleSuccessModal = (successString) => {
    this.fetchData();
    this.setState({
      showSuccessModal: !this.state.showSuccessModal,
      successString,
    });
  }

  toggleInfoModal = (infoMessage, isErrInfo = false) => {
    this.setState({
      showInfoModal: !this.state.showInfoModal,
      infoMessage,
      isErrInfo,
    });
  }

  openRemoveUserModal = (row) => {
    const { username } = row;
    this.setState({
      showWarningModal: true,
      warningMessage: `${'Are you sure want to delete<b>'} ${username}${'</b>? This action can’t be undone.'}`,
      user: Object.assign({}, row),
    });
  };

  toggleWarningModal = () => {
    this.setState({
      showWarningModal: false,
      warningMessage: '',
    });
  };

  _handleDeleteUser = () => {
    const { id } = this.state.user;
    API.delete(`${config.routes.user.delete}/${id}`).then(() => {
      this.setState({
        showWarningModal: false,
        showInfoModal: true,
        infoMessage: 'User deleted!',
      });
      this.fetchData('');
    });
  }

  render() {
    const {
      showCreateUserModal,
      showStatusModal,
      showUpdateUserModal,
      showSuccessModal,
      showInfoModal,
      data,
      perPage,
      currentPage,
      totalItems,
      user,
      isLoading,
      options,
      currentUser,
      infoMessage,
      isErrInfo,
      successString,
      showWarningModal,
      warningMessage,
    } = this.state;
    const { type } = this.props;
    return (
      <div>
        <SideBar type={type} />
        <div id="page-wrapper">
          <Header type={type} history={this.props.history} />
          <div className="page">
            {
              isLoading && <LoadingSpinner isLoading={isLoading} />
            }
            <div className="page__header">
              <div className="page__header__left">
                <span className="page__header__title">Users</span>
                <span className="page__header__total">Total: {totalItems}</span>
              </div>

              <div className="page__header__right">
                <div className="search__form">
                  <FormGroup>
                    <InputGroup>
                      <DebounceInput
                        minLength={2}
                        debounceTimeout={1000}
                        onChange={this.onSearch}
                        placeholder="Search"
                      />
                      <InputGroup.Addon>
                        <i className="icon-search" />
                      </InputGroup.Addon>
                    </InputGroup>
                  </FormGroup>
                </div>

                {currentUser.typeId === 1 ? null : (
                  <button
                    className="fgs__btn m__l--16 bg--secondary-aquamarine"
                    onClick={this.openCreateUserModal}
                  >
                    <i className="icon-add font__size--24 m__r--8" />
                    <span>Create User</span>
                  </button>)
                }
              </div>
            </div>
            <UserTable
              data={data}
              perPage={perPage}
              currentPage={currentPage}
              openStatusModal={this.openStatusModal}
              openUpdateUserModal={this.openUpdateUserModal}
              openRemoveUserModal={this.openRemoveUserModal}
              total={totalItems}
              onChangePerPage={this.onChangePerPage}
              onChangePage={this.onChangePage}
              options={options}
              currentUser={currentUser}
              disableActionColumn={currentUser.typeId === 1}
            />

            {showCreateUserModal && <CreateUserModal
              show={showCreateUserModal}
              onHide={this.closeCreateUserModal}
              onComplete={this.fetchData}
              showSuccessModal={this.toggleSuccessModal}
              showInfoModal={this.toggleInfoModal}
            />}

            {showStatusModal && <StatusModal
              show={showStatusModal}
              onHide={this.closeStatusModal}
            />}

            {showUpdateUserModal && <UpdateUserModal
              show={showUpdateUserModal}
              onHide={this.closeUpdateUserModal}
              user={user}
              onComplete={this.fetchData}
            />}

            {showSuccessModal && <SuccessModal
              show={showSuccessModal}
              onHide={this.toggleSuccessModal}
              message={successString}
            />}

            {showInfoModal && <InfoModal
              show={showInfoModal}
              onHide={this.toggleInfoModal}
              infoMessage={infoMessage}
              isErrInfo={isErrInfo}
            />}

            {showWarningModal && <WarningModal
              show={showWarningModal}
              onHide={this.toggleWarningModal}
              message={warningMessage}
              onSubmit={this._handleDeleteUser}
              btnConfirmText="Delete"
              btnCancelText="Cancel"
            />}
          </div>
        </div>
      </div>
    );
  }
}

export default User;
