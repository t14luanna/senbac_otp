import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { DropdownButton, MenuItem } from 'react-bootstrap';

import Pagination from '../../../../components/Pagination/Pagination';
// eslint-disable-next-line import/first
import moment from 'moment';

class UserTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sizePerPage: this.props.perPage,
      currentPage: this.props.currentPage,
    };
  }

  onChangePage = (page) => {
    this.setState({ currentPage: page });
    this.props.onChangePage(page);
  };

  onChangePerPage = (size) => {
    this.setState({ sizePerPage: size, currentPage: 1 });
    this.props.onChangePerPage(size);
    this.props.onChangePage(1);
  };

  nameFormatter = (cell, row) => {
    const { userName } = row;
    return <p>{userName}</p>;
  };

  noFormatter = (cell, row) => {
    const { currentPage, sizePerPage } = this.state;
    const { data } = this.props;

    return <p>{sizePerPage * (currentPage - 1) + data.indexOf(row) + 1}</p>;
  };

  usernameFormatter = (cell, row) => {
    return (
      <div>
        <div className="fgs__box m__b--0">
          <label className="fgs__box__label">Username</label>
          <p>{row}</p>
        </div>
      </div>
    );
  };

  infoFormatter = (cell, row) => {
    return (
      <div>
        <div className="fgs__box m__b--0">
          <label className="fgs__box__label">Email</label>
          <p>{row.email}</p>
        </div>
        <div className="fgs__box m__b--0">
          <label className="fgs__box__label">Full Name</label>
          <p>{row.fullName}</p>
        </div>
      </div>
    );
  };

  aesFormatter = (cell, row) => {
    return (
      <div>
        <div className="fgs__box m__b--0">
          <label className="fgs__box__label">KeyAES</label>
          <p>{row.keyAES}</p>
        </div>
        <div className="fgs__box m__b--0">
          <label className="fgs__box__label">IvAES</label>
          <p>{row.ivAES}</p>
        </div>
      </div>
    );
  };


  actionFormatter = (cell, row) => {
    return (
      <div className="fgs__action__dropdown">
        <DropdownButton
          bsSize="large"
          title="Actions"
          pullRight
          id="dropdown-size-large"
          className="fgs__dropdown-toggle"
        >
          <MenuItem
            eventKey="1"
            onClick={() => this.props.openUpdateUserModal(row)}
          >
            Edit information
          </MenuItem>
          <MenuItem
            eventKey="2"
            onClick={() => this.props.openRemoveUserModal(row)}
            className="remove__item"
          >
            Remove user
          </MenuItem>
        </DropdownButton>
      </div>
    );
  };

  statusFormatter = (cell, row) => {
    return (
      <div>
        <div className={`fgs__box ${!row.activated ? 'm__b--0' : ''}`}>
          <label className="odc__box__label">Activated</label>
          <p>{row.activated !== '' ? 'Activated' : 'Deactivated'}</p>
        </div>
        <div className="fgs__box m__b--0">
          <label className="fgs__box__label">Language</label>
          <p>{row.language}</p>
        </div>
        <div className="fgs__box m__b--0">
          <label className="fgs__box__label">Created At</label>
          <p>{moment(row.createdAt).format('DD/MM/YYYY - hh:mm:ss')}</p>
        </div>
        <div className="fgs__box m__b--0">
          <label className="fgs__box__label">Updated At</label>
          <p>{moment(row.updatedAt).format('DD/MM/YYYY - hh:mm:ss')}</p>
        </div>
      </div>
    );
  };

  render() {
    const { currentPage, sizePerPage } = this.state;
    const { data, options, disableActionColumn } = this.props;
    return (
      <div className="table__container">
        <BootstrapTable data={data} options={options}>
          <TableHeaderColumn
            isKey
            dataField="id"
            width="100"
            dataFormat={this.noFormatter}
          >
            N°
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="username"
            width="100"
            dataFormat={this.usernameFormatter}
          >
            USERNAME
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="info"
            width="100"
            dataFormat={this.infoFormatter}
          >
            INFO
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="aes"
            width="100"
            dataFormat={this.aesFormatter}
          >
            AES
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="status"
            width="100"
            dataFormat={this.statusFormatter}
          >
            STATUS
          </TableHeaderColumn>
          {disableActionColumn ? null :
          <TableHeaderColumn width="200" dataFormat={this.actionFormatter}>
              Actions
          </TableHeaderColumn>
          }
        </BootstrapTable>
        <Pagination
          start={(currentPage - 1) * sizePerPage + 1}
          to={(currentPage - 1) * sizePerPage + sizePerPage}
          total={this.props.total}
          sizePerPage={sizePerPage}
          currentPage={currentPage}
          onChangePage={this.onChangePage}
          onChangePerPage={this.onChangePerPage}
        />
      </div>
    );
  }
}

export default UserTable;
