import React from 'react';
import 'react-select/dist/react-select.css';
import './Parameters.scss';
import { API } from '../../../helpers';
import SideBar from '../../../layout/SideBar/SideBar';
import Header from '../../../layout/Header/Header';
import { isContainSpecialChar, isContainCapital, isContainLowerCase, isContainNumber } from '../../../utils/utils';
import SuccessModal from '../../../components/Modal/SuccessModal/SuccessModal';
import config from '../../../config';

class Parameters extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      errString: '',
      errFields: [],
      currentPass: '',
      newPass: '',
      confirmNewPass: '',
      showSuccessModal: false,
      successMessage: '',
    };
  }

  handleInputChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      errString: '',
      errFields: [],
    });
  }

  passwordValidate = (password) => {
    const validations = [{
      validator: () => password.length >= 8,
      errMessage: 'New password must have at least 8 character',
    }, {
      validator: () => isContainCapital(password),
      errMessage: 'New password must have at least 1 capital letter',
    }, {
      validator: () => isContainLowerCase(password),
      errMessage: 'New password must have at least 1 lowercase letter',
    },
    {
      validator: () => isContainSpecialChar(password),
      errMessage: 'New password must have at least 1 special character',
    },
    {
      validator: () => isContainNumber(password),
      errMessage: 'New password must have at least 1 number',
    }];
    for (let i = 0; i < validations.length; i += 1) {
      if (!validations[i].validator()) {
        return validations[i].errMessage;
      }
    }
    return '';
  }

  handleSubmit = () => {
    const { currentPass, newPass, confirmNewPass } = this.state;
    if (currentPass.length < 1) {
      const passwordError = 'Password is required';
      this.setState({ errString: passwordError, errFields: [0] });
    } else {
      const passwordError = this.passwordValidate(newPass);
      if (passwordError) {
        this.setState({ errString: passwordError, errFields: [1] });
      } else if (newPass !== confirmNewPass) {
        this.setState({ errString: 'Confirmed new password is not match', errFields: [1, 2] });
      } else {
        API.post(config.routes.editUserPassword, { oldPassword: currentPass, newPassword: newPass, confirmNewPass }).then(() => {
          this.setState({
            errString: '',
            errFields: [],
            currentPass: '',
            newPass: '',
            confirmNewPass: '',
          });
          this.toggleSuccessModal('Change password successfully!');
        }).catch((err) => {
          this.setState({ errString: 'Wrong password', errFields: [0] });
        });
      }
    }
  }

  toggleSuccessModal = (successMessage) => {
    this.setState({
      showSuccessModal: !this.state.showSuccessModal,
      successMessage,
    });
  }

  render() {
    const {
      type,
    } = this.props;

    const {
      currentPass,
      newPass,
      confirmNewPass,
      errString,
      errFields,
      showSuccessModal,
      successMessage,
    } = this.state;

    return (
      <React.Fragment>
        <SideBar type={type} />
        <div id="page-wrapper">
          <Header type={type} history={this.props.history} />
          <div className="page page__parameters">
            <div className="page__header m__b--0">
              <div className="page__header__left">
                <span className="page__header__title">Parameters</span>
              </div>
            </div>
            <div className="page__body">
              <h2 className="page__body__title">Change my password</h2>

              <div className="row">
                <div className="col-md-6">
                  <div className="fgs__form__group">
                    <label htmlFor="currentPass" className="fgs__box__label m__b--8">Current password</label>
                    <div className={`fgs__input__container ${errFields && errFields.includes(0) && 'err'}`}>
                      <input
                        className="err fgs__input__control"
                        type="password"
                        placeholder="Current password"
                        name="currentPass"
                        value={currentPass}
                        onChange={this.handleInputChange}
                        required
                      />
                      <i className="icon-info" />
                    </div>
                  </div>

                  <div className="fgs__form__group">
                    <label htmlFor="newPass" className="fgs__box__label m__b--8">New password</label>
                    <div className={`fgs__input__container ${errFields && errFields.includes(1) && 'err'}`}>
                      <input
                        type="password"
                        placeholder="New password"
                        name="newPass"
                        value={newPass}
                        required
                        onChange={this.handleInputChange}
                        className="fgs__input__control"
                      />
                      <i className="icon-info" />
                    </div>

                  </div>

                  <div className="fgs__form__group">
                    <label htmlFor="confirmNewPass" className="fgs__box__label m__b--8">Confirm new password</label>
                    <div className={`fgs__input__container ${errFields && errFields.includes(2) && 'err'}`}>
                      <input
                        type="password"
                        placeholder="Confirm new password"
                        name="confirmNewPass"
                        value={confirmNewPass}
                        required
                        onChange={this.handleInputChange}
                        className="fgs__input__control"
                      />
                      <i className="icon-info" />
                    </div>
                  </div>

                  {errString && <p className="text--error font__size--10">{errString}</p>}

                  <button
                    className="fgs__btn fgs__btn--large bg--secondary-aquamarine min__w--124 m__t--40"
                    onClick={this.handleSubmit}
                  >
                    Update
                  </button>
                </div>
              </div>
            </div>
            {showSuccessModal && <SuccessModal
              show={showSuccessModal}
              onHide={this.toggleSuccessModal}
              information={successMessage}
            />}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Parameters;
