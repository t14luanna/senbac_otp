const config = {
  API_HOST: 'https://otp.senbac.com:8080',
  routes: {
    login: '/user/signin',
    signout: '/user/signout',
    checkToken: '/user/check',
    editUserPassword: '/user/user_change_password',
    user: {
      query: '/user/get_all',
      create: '/user/create',
      update: '/user/update',
      delete: '/user/remove',
    },
  },
};

export default config;
