import moment from 'moment';

export function getGreetingTime(currInputTime) {
  const currentTime = moment(currInputTime);

  if (!currentTime || !currentTime.isValid()) { return 'Hello'; }

  const splitAfternoon = 12; // 24hr time to split the afternoon
  const splitEvening = 17; // 24hr time to split the evening
  const currentHour = parseFloat(currentTime.format('HH'));

  if (currentHour >= splitAfternoon && currentHour <= splitEvening) {
    // Between 12 PM and 5PM
    return 'Good afternoon';
  } else if (currentHour >= splitEvening) {
    // Between 5PM and Midnight
    return 'Good evening';
  }
  // Between dawn and noon
  return 'Good morning';
}

export function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

export function calculateRemainingDays(endDateTimeStamp) {
  const startDate = moment(new Date());
  const endDate = moment(new Date(endDateTimeStamp));
  const duration = moment.duration(startDate.diff(endDate));
  const days = duration.days();
  return `${days} ${days > 1 ? 'days' : 'day'}`;
}

export const isValidEmail = (email) => {
  const pattern = new RegExp(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/);// eslint-disable-line
  return pattern.test(email);
};

export const isValidAddress = (address) => {
  // allow space, '-' , ',' ...
  const pattern = new RegExp(/[~!@#$%^&*()_\+\=\[\]{}\\|;':"<>?/]/);// eslint-disable-line
  return !pattern.test(address);
};

export const isContainSpecialCharInName = (name) => {
  const pattern = new RegExp(/[~!@#$%^&*()_\+\=\[\]{}\\|;':"<>?,./]/);// eslint-disable-line
  return pattern.test(name);
};

export const isContainSpecialChar = (name) => {
  const pattern = new RegExp(/[#@&é§!çà\\)\\(\"'\\/\\-_*$€¨^%ù£`\\+=/:\\.;\\?\\,]/);// eslint-disable-line
  return pattern.test(name);
};

// can be '+' in the first position (for phone number)
export const isNumberOnly = (input) => {
  const pattern = new RegExp(/^[+0-9]+$/);
  return pattern.test(input);
};

export const isContainCapital = (input) => {
  const pattern = new RegExp(/[A-Z]/);
  return pattern.test(input);
};

export const isContainLowerCase = (input) => {
  const pattern = new RegExp(/[a-z]/);
  return pattern.test(input);
};

export const isContainNumber = (input) => {
  const pattern = new RegExp(/[0-9]/);
  return pattern.test(input);
};

export const isValidPhoneNumber = (input) => {
  if (!isNumberOnly(input)) {
    return false;
  }

  if (!(!input.startsWith('0') && input.length === 9) && !(input.startsWith('0') && input.length === 10)) {
    return false;
  }

  return true;
};

export const isValidPassword = (input) => {
  const passwordLength = input.length;
  if (passwordLength > 254 || passwordLength < 6) {
    return {
      isError: true,
      message: 'Password must be at least 6 characters',
    };
  }

  const challenge1 = new RegExp(/.*[A-Z]{1}.*/);
  if (!challenge1.test(input)) {
    return {
      isError: true,
      message: '1 majuscule requise',
    };
  }

  const challenge2 = new RegExp(/.*[a-z]{1}.*/);
  if (!challenge2.test(input)) {
    return {
      isError: true,
      message: '1 minuscule requise',
    };
  }

  const challenge3 = new RegExp(/.*[0-9]{1}.*/);
  if (!challenge3.test(input)) {
    return {
      isError: true,
      message: '1 chiffre requis',
    };
  }

  const challenge4 = new RegExp(/.*[#@&é§!çà\\)\\("'\\/\\-_*$€¨^%ù£`\\+=/:\\.;\\?\\,]{1}.*/);
  if (!challenge4.test(input)) {
    return {
      isError: true,
      message: '1 caractère special requis',
    };
  }
  return {
    isError: false,
  };
};
