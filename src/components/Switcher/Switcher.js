import React from 'react';
import './Switcher.scss';

const Switcher = ({ onToggle, checked, description }) => (
  <label className="switcher">
    <input
      type="checkbox"
      checked={checked}
      onChange={onToggle}
    />
    {
      description ? (<p>{description}</p>) : null
    }
    <span />
  </label>
);


export default Switcher;
