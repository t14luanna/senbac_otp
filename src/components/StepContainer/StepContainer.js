import React from 'react';

import './StepContainer.scss';

class StepContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: props.defaultStep ? props.defaultStep : 0,
    };
  }

  setActiveStep = (activeStep) => {
    this.setState({ activeStep });
  }

  renderListStep = (steps) => {
    const { activeStep } = this.state;
    const { defaultStep } = this.props;
    return (
      <div className="steps__container" >
        {
          steps.map((step, index) => {
            return (
              <div className={`step__item ${activeStep >= index + defaultStep && 'active'}`} key={`${step}`}>
                <div className="step__item__content">
                  <div className="step__item__inner">
                    <span className="oval">{activeStep > index + defaultStep ? (<span className="checkmark" />) : index + 1}</span>
                  </div>
                </div>
              </div>
            );
          })
        }
      </div>
    );
  }

  renderStepsText = (steps) => {
    return (
      <div className="steps__container__text">
        {
          steps.map((step) => {
            return (
              <span className="text__item" key={step}>{step}</span>
            );
          })
        }
      </div>
    );
  }

  render() {
    const { steps } = this.props;
    return (
      <React.Fragment>
        { this.renderListStep(steps) }
        { this.renderStepsText(steps) }
      </React.Fragment>
    );
  }
}

export default StepContainer;
