import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';

class WarningModal extends Component {
  render() {
    const {
      show,
      onHide,
      message,
      onSubmit,
      btnConfirmText,
      btnCancelText,
    } = this.props;

    return (
      <Modal id="warning-modal" show={show} onHide={onHide} className="fgs__modal fgs__modal--small">
        <Modal.Body className="text-center">
          <button type="button" className="fgs__close__btn" data-dismiss="modal" aria-label="Close" onClick={onHide}>
            <i className="icon-close" />
          </button>
          <i className="icon-warning fgs__modal__icon d__block text--warning" />
          <div className="fgs__modal__message" dangerouslySetInnerHTML={{ __html: message }} />
          <div className="fgs__btn__groups">
            <button className="fgs__btn fgs__btn--large fgs__btn--primary--outline" onClick={onHide}>{btnCancelText}</button>
            <button className={`fgs__btn fgs__btn--large ${btnConfirmText === 'Activate' ? 'bg--secondary-aquamarine' : 'bg--danger'}`} onClick={onSubmit}>{btnConfirmText}</button>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

export default WarningModal;
