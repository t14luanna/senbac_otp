import React from 'react';
import { Modal } from 'react-bootstrap';

import './ErrorModal.scss';

class ErrorModal extends React.PureComponent {
  render() {
    const {
      show,
      onHide,
      message,
      btnCancelText,
      title,
    } = this.props;
    return (
      <Modal id="error-modal" show={show} onHide={onHide}>
        <Modal.Body>
          <div className="content-container">
            <i className="icon-error" />
            {title && <div className="title-text">{title}</div>}
            {message && <div className="information-text">{message}</div>}
            <div className="buttons">
              <button className="custome-btn custome-btn-cancel" onClick={onHide}>{btnCancelText}</button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

export default ErrorModal;
