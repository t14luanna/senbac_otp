import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import './InforModal.scss';

class InfoModal extends Component {
  render() {
    const {
      show,
      onHide,
      infoMessage,
      isErrInfo,
    } = this.props;

    return (
      <Modal id="info-modal" show={show} onHide={onHide} className="fgs__modal fgs__modal--small">
        <Modal.Body className="text-center">
          <button type="button" className="fgs__close__btn" data-dismiss="modal" aria-label="Close" onClick={onHide}>
            <i className="icon-close" />
          </button>
          <i className={`icon-info fgs__modal__icon d__block ${isErrInfo ? 'text--error' : 'text--info'}`} />
          <div className="fgs__modal__message" dangerouslySetInnerHTML={{ __html: infoMessage }} />
        </Modal.Body>
      </Modal>
    );
  }
}

export default InfoModal;
