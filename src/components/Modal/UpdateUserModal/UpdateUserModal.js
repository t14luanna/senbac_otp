import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import 'react-select/dist/react-select.css';

import SuccessModal from '../SuccessModal/SuccessModal';

import { API } from '../../../helpers';
import config from '../../../config';
import Checkbox from '../../Checkbox/Checkbox';

class UpdateUserModal extends Component {
  constructor(props, context) {
    super(props, context);
    const { user } = props;
    this.state = {
      id: user.id,
      username: user.username,
      activated: user.activated,
      keyAES: user.keyAES,
      ivAES: user.ivAES,
      trunks: user.trunks,
      email: user.email,
      fullName: user.fullName,
      errors: [],
    };
  }

  onInputChange = (e) => {
    this.setState({ [e.target.name]: e.target.value, errors: [] });
  }

  onSubmit = async () => {
    const errors = this.validateInput();
    const {
      id,
      username,
      activated,
      keyAES,
      ivAES,
      trunks,
      email,
      fullName,
    } = this.state;

    if (errors.length > 0) {
      this.setState({ errors });
      return;
    }
    this.setState({ errors: [] });
    try {
      await API.put(`${config.routes.user.update}`, {
        id,
        username,
        activated,
        keyAES,
        ivAES,
        trunks,
        email,
        fullName,
      });
      this.openSuccessModal('The information has been successfully updated!');
    } catch (e) {
      // TODO: handle error
    }
  }

  openSuccessModal = (successMessage) => {
    this.setState({
      showSuccessModal: true,
      successMessage,
    });
  }

  closeSuccessModal = () => {
    this.props.onComplete();
    this.props.onHide();
    this.setState({ showSuccessModal: false });
  }

  validateInput = () => {
    const {
      username, keyAES, ivAES,
    } = this.state;

    let errors = [];

    const checkEmpty = (fieldValue, fieldName) => {
      if (!fieldValue) {
        errors = [...errors, `${fieldName} is required!`];
      }
    };

    checkEmpty(username, 'User');
    checkEmpty(keyAES, 'keyAES');
    checkEmpty(ivAES, 'ivAES');

    return errors;
  }

  render() {
    const { show, onHide } = this.props;
    const {
      username,
      activated,
      keyAES,
      ivAES,
      trunks,
      email,
      fullName,
      errors,
      showSuccessModal,
      successMessage,
    } = this.state;

    return (
      <React.Fragment>
        <Modal
          id="update-user-modal"
          className={`fgs__modal ${showSuccessModal ? 'success' : ''}`}
          show={show}
          onHide={onHide}
        >
          <Modal.Header>
            <button type="button" className="fgs__close__btn" data-dismiss="modal" aria-label="Close" onClick={onHide}>
              <i className="icon-close" />
            </button>
            <p className="fgs__modal__title text-center">Edit user information</p>
          </Modal.Header>
          <Modal.Body>
            {
              errors.length > 0 &&
              <div className="text-center m__b--16">
                {errors.map(err => <p className="text--error font__size--10" key={err}>{err}</p>)}
              </div>
            }
            <div className="fgs__form__group">
              <label htmlFor="username" className="fgs__box__label m__b--8">Name *</label>
              <div className="fgs__input__container">
                <input
                  name="username"
                  className={`fgs__input__control ${username && 'has-value'}`}
                  type="text"
                  placeholder="Username"
                  onChange={this.handleChange}
                  value={username}
                  maxLength={254}
                />
              </div>
            </div>
            <div className="fgs__form__group">
              <Checkbox
                name="Activated"
                checked={activated}
                onChange={this.handleChangeActivated}
                label="Activated"
              />
            </div>
            {/* <div className="fgs__form__group">
              <label htmlFor="plainPassword" className="fgs__box__label m__b--8">Password *</label>
              <div className="fgs__input__container">
                <input
                  name="plainPassword"
                  className={`fgs__input__control ${plainPassword && 'has-value'}`}
                  type="password"
                  placeholder="Password"
                  onChange={this.handleChange}
                  value={plainPassword}
                  maxLength={254}
                />
              </div>
            </div> */}
            <div className="fgs__form__group">
              <label htmlFor="fullName" className="fgs__box__label m__b--8">Full Name</label>
              <div className="fgs__input__container">
                <input
                  name="fullName"
                  className={`fgs__input__control ${fullName && 'has-value'}`}
                  type="fullName"
                  placeholder="Full Name"
                  onChange={this.handleChange}
                  value={fullName}
                  maxLength={254}
                />
              </div>
            </div>
            <div className="fgs__form__group">
              <label htmlFor="email" className="fgs__box__label m__b--8">Email</label>
              <div className="fgs__input__container">
                <input
                  name="email"
                  className={`fgs__input__control ${email && 'has-value'}`}
                  type="email"
                  placeholder="Email"
                  onChange={this.handleChange}
                  value={email}
                  maxLength={254}
                />
              </div>
            </div>
            <div className="fgs__form__group">
              <label htmlFor="keyAES" className="fgs__box__label m__b--8">KeyAES *</label>
              <div className="fgs__input__container">
                <input
                  name="keyAES"
                  className={`fgs__input__control ${keyAES && 'has-value'}`}
                  type="text"
                  placeholder="KeyAES"
                  onChange={this.handleChange}
                  value={keyAES}
                  maxLength={254}
                />
              </div>
            </div>
            <div className="fgs__form__group">
              <label htmlFor="ivAES" className="fgs__box__label m__b--8">IvAES *</label>
              <div className="fgs__input__container">
                <input
                  name="ivAES"
                  className={`fgs__input__control ${ivAES && 'has-value'}`}
                  type="text"
                  placeholder="IvAES"
                  onChange={this.handleChange}
                  value={ivAES}
                  maxLength={254}
                />
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer className="fgs__btn__groups">
            <button className="fgs__btn fgs__btn--large fgs__btn--primary--outline" onClick={onHide}>Cancel</button>
            <button className="fgs__btn fgs__btn--large bg--secondary-aquamarine" onClick={this.onSubmit}>Save</button>
          </Modal.Footer>
        </Modal >

        {showSuccessModal && <SuccessModal
          show={showSuccessModal}
          onHide={this.closeSuccessModal}
          message={successMessage}
        />}
      </React.Fragment>
    );
  }
}

export default UpdateUserModal;
