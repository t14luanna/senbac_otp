import React, { Component } from 'react';
import './index.scss';
import Checkbox from '../../../Checkbox/Checkbox';
import { isContainCapital, isContainLowerCase, isContainSpecialChar, isContainNumber } from '../../../../utils/utils';

// eslint-disable-next-line react/no-multi-comp
export default class InformationStep extends Component {
  constructor(props) {
    super(props);
    const user = { ...props.newUser };
    this.state = {
      ...user,
      username: '',
      plainPassword: '',
      activated: false,
      keyAES: '',
      ivAES: '',
      trunks: [],
      email: '',
      fullName: '',
    };
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.name === this.state.errorType) {
      this.setState({ errorMessage: '', errorType: '' });
    }
  }

  handleChangeActivated = () => {
    const { activated } = this.state;
    this.setState({ activated: !activated });
  }

  passwordValidate = (password) => {
    const validations = [{
      validator: () => password.length >= 8,
      errMessage: 'New password must have at least 8 character',
    }, {
      validator: () => isContainCapital(password),
      errMessage: 'New password must have at least 1 capital letter',
    }, {
      validator: () => isContainLowerCase(password),
      errMessage: 'New password must have at least 1 lowercase letter',
    },
    {
      validator: () => isContainSpecialChar(password),
      errMessage: 'New password must have at least 1 special character',
    },
    {
      validator: () => isContainNumber(password),
      errMessage: 'New password must have at least 1 number',
    }];
    for (let i = 0; i < validations.length; i += 1) {
      if (!validations[i].validator()) {
        return validations[i].errMessage;
      }
    }
    return '';
  }

  validateInput = () => {
    this.setState({ errorMessage: '', errorType: '' });
    const {
      username, plainPassword, keyAES, ivAES,
    } = this.state;

    if (!username) {
      this.setState({ errorMessage: 'Please select name', errorType: 'name' });
      return false;
    }

    const passwordError = this.passwordValidate(plainPassword);
    if (passwordError) {
      this.setState({ errorMessage: passwordError, errorType: 'plainPassword' });
    }

    if (!keyAES) {
      this.setState({ errorMessage: 'Please select keyAES', errorType: 'keyAES' });
      return false;
    }

    if (!ivAES) {
      this.setState({ errorMessage: 'Please select ivAES', errorType: 'ivAES' });
      return false;
    }

    this.updateParent();
    return true;
  }

  updateParent = () => {
    const {
      username, plainPassword, keyAES, ivAES, activated, email, fullName, trunks,
    } = this.state;

    this.props.handleNewUserChange({
      username, plainPassword, keyAES, ivAES, activated, email, fullName, trunks,
    });
  }

  render() {
    const {
      username,
      plainPassword,
      activated,
      keyAES,
      ivAES,
      trunks,
      email,
      fullName,
      errorMessage,
      errorType,
    } = this.state;

    return (
      <div className="information-step">
        <div className="fgs__form__group">
          <label htmlFor="username" className="fgs__box__label m__b--8">Name *</label>
          <div className="fgs__input__container">
            <input
              name="username"
              className={`fgs__input__control ${username && 'has-value'}`}
              type="text"
              placeholder="Username"
              onChange={this.handleChange}
              value={username}
              maxLength={254}
            />
          </div>
          <label htmlFor="username" className="fgs__box__label m__b--8 error-message">{errorType === 'username' ? errorMessage : ''}</label>
        </div>
        <div className="fgs__form__group">
          <Checkbox
            name="Activated"
            checked={activated}
            onChange={this.handleChangeActivated}
            label="Activated"
          />
        </div>
        <div className="fgs__form__group">
          <label htmlFor="plainPassword" className="fgs__box__label m__b--8">Password *</label>
          <div className="fgs__input__container">
            <input
              name="plainPassword"
              className={`fgs__input__control ${plainPassword && 'has-value'}`}
              type="password"
              placeholder="Password"
              onChange={this.handleChange}
              value={plainPassword}
              maxLength={254}
            />
          </div>
          <label htmlFor="plainPassword" className="fgs__box__label m__b--8 error-message">{errorType === 'plainPassword' ? errorMessage : ''}</label>
        </div>
        <div className="fgs__form__group">
          <label htmlFor="fullName" className="fgs__box__label m__b--8">Full Name</label>
          <div className="fgs__input__container">
            <input
              name="fullName"
              className={`fgs__input__control ${fullName && 'has-value'}`}
              type="fullName"
              placeholder="Full Name"
              onChange={this.handleChange}
              value={fullName}
              maxLength={254}
            />
          </div>
          <label htmlFor="fullName" className="fgs__box__label m__b--8 error-message">{errorType === 'fullName' ? errorMessage : ''}</label>
        </div>
        <div className="fgs__form__group">
          <label htmlFor="email" className="fgs__box__label m__b--8">Email</label>
          <div className="fgs__input__container">
            <input
              name="email"
              className={`fgs__input__control ${email && 'has-value'}`}
              type="email"
              placeholder="Email"
              onChange={this.handleChange}
              value={email}
              maxLength={254}
            />
          </div>
          <label htmlFor="email" className="fgs__box__label m__b--8 error-message">{errorType === 'email' ? errorMessage : ''}</label>
        </div>
        <div className="fgs__form__group">
          <label htmlFor="keyAES" className="fgs__box__label m__b--8">KeyAES *</label>
          <div className="fgs__input__container">
            <input
              name="keyAES"
              className={`fgs__input__control ${keyAES && 'has-value'}`}
              type="text"
              placeholder="KeyAES"
              onChange={this.handleChange}
              value={keyAES}
              maxLength={254}
            />
          </div>
          <label htmlFor="keyAES" className="fgs__box__label m__b--8 error-message">{errorType === 'keyAES' ? errorMessage : ''}</label>
        </div>
        <div className="fgs__form__group">
          <label htmlFor="ivAES" className="fgs__box__label m__b--8">IvAES *</label>
          <div className="fgs__input__container">
            <input
              name="ivAES"
              className={`fgs__input__control ${ivAES && 'has-value'}`}
              type="text"
              placeholder="IvAES"
              onChange={this.handleChange}
              value={ivAES}
              maxLength={254}
            />
          </div>
          <label htmlFor="ivAES" className="fgs__box__label m__b--8 error-message">{errorType === 'ivAES' ? errorMessage : ''}</label>
        </div>
        <div className="tip">* This field is required</div>
      </div>
    );
  }
}
