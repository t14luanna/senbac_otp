import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import InformationStep from './InformationStep';
import SuccessModal from '../SuccessModal/SuccessModal';

import { API } from '../../../helpers';
import config from '../../../config';

const initState = {
  newUser: {
  },
};

class CreateUserModal extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = initState;
    this.informationComponent = React.createRef();
  }

  componentDidMount() {
    this.setState(initState);
  }

  onDone = () => {
    if (this.props.onHide) {
      this.props.onHide();
    }
  };

  onCreateMore = () => {
    this.setState({ showSuccessModal: false });
    this.setState(initState);
  }

  validate = () => {
    const self = this;
    return new Promise((resolve, reject) => {
      if (self.informationComponent.current.validateInput()) {
        return resolve();
      }
      return reject();
    });
  };

  handleNewUserChange = (results) => {
    this.setState({
      newUser: { ...results },
    });
  };

  openSuccessModal = () => {
    this.setState({
      showSuccessModal: true,
    });
  };

  closeSuccessModal = () => {
    this.setState({ showSuccessModal: false });
    this.onDone();
  };

  saveUser = async () => {
    this.validate().then(() => {
      const { newUser } = this.state;

      API.post(config.routes.user.create, newUser).then((res) => {
        if (res.data.success) {
          this.props.onHide();
          this.props.showSuccessModal('Congratulations! You have just created user successfully!');
        } else {
          this.props.showInfoModal('Failed to create user!', true);
        }
      });
    });
  }

  cancleCreate = () => {
    this.props.onHide();
  }

  render() {
    const {
      newUser,
      showSuccessModal,
    } = this.state;
    const { show, onHide } = this.props;
    return (
      <React.Fragment >
        <Modal
          id="create-entity-form"
          className={`fgs__modal fgs__modal--step ${showSuccessModal ? 'success' : ''}`}
          show={show}
          onHide={onHide}
          backdrop="static"
        >
          <Modal.Header>
            <button
              type="button"
              className="fgs__close__btn"
              data-dismiss="modal"
              aria-label="Close"
              onClick={onHide}
            >
              <i className="icon-close" />
            </button>
            <p className="fgs__modal__title text-center">Create user information</p>
          </Modal.Header>
          <Modal.Body>
            <InformationStep
              ref={this.informationComponent}
              newUser={newUser}
              handleNewUserChange={this.handleNewUserChange}
            />
          </Modal.Body>
          <Modal.Footer className="fgs__btn__groups">
            <button className="prev-step fgs__btn fgs__btn--large fgs__btn--primary--outline" onClick={this.cancleCreate}>
              Cancel
            </button>
            <button className="next-step fgs__btn fgs__btn--large bg--secondary-aquamarine" onClick={this.saveUser}>
              Complete
            </button>
          </Modal.Footer>
        </Modal>
        {showSuccessModal && <SuccessModal
          show={showSuccessModal}
          onHide={this.closeSuccessModal}
          message="Create user successfully!"
          onPrev={this.closeSuccessModal}
          prevText="Close"
          onNext={this.onCreateMore}
          nextText="Create more"
        />}
      </React.Fragment>
    );
  }
}

export default CreateUserModal;
