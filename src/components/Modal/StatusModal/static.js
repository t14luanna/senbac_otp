const status = [
  {
    package: 'Basic - 3 months',
    times: 2,
    date: ['22 Jan 2018', '21 Sep 2017'],
  },
  {
    package: 'Premium - 6 months',
    times: 1,
    date: ['23 Mar 2018'],
  },
  {
    package: 'SVIP - 12 months',
    times: 0,
    date: [],
  },
];

export default status;
