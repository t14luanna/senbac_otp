import React, { Component } from 'react';
import uuid from 'uuid';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Modal } from 'react-bootstrap';

import './StatusModal.scss';

import status from './static';

function formatter(cell, row) {
  return <div>{cell.map(item => <p key={uuid.v4()}>{item}</p>)}</div>;
}

class StatusModal extends Component {
  render() {
    const { show, onHide } = this.props;
    return (
      <Modal id="status" show={show} onHide={onHide}>
        <Modal.Body>
          <div>
            <button
              type="button"
              className="button-close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={onHide}
            >
              <i className="icon-close" />
            </button>
          </div>

          <BootstrapTable data={status}>
            <TableHeaderColumn dataField="package" width="180" isKey>
              Packaging
            </TableHeaderColumn>
            <TableHeaderColumn dataField="times" width="120">
              Times
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="date"
              width="140"
              dataFormat={formatter}
            >
              Date
            </TableHeaderColumn>
          </BootstrapTable>
        </Modal.Body>
      </Modal>
    );
  }
}
export default StatusModal;
