import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import 'react-select/dist/react-select.css';

class SuccessModal extends Component {
  render() {
    const {
      show,
      onHide,
      message,
      onPrev,
      onNext,
      prevText,
      nextText,
    } = this.props;

    return (
      <Modal id="success-modal" show={show} onHide={onHide} className="fgs__modal fgs__modal--small">
        <Modal.Body className="text-center">
          <button
            type="button"
            className="fgs__close__btn"
            data-dismiss="modal"
            aria-label="Close"
            onClick={onHide}
          >
            <i className="icon-close" />
          </button>
          <i className="icon-success fgs__modal__icon d__block text--success" />
          <p className="fgs__modal__message">{message}</p>
          {(onPrev || onNext) && (
            <div className="fgs__btn__groups">
              {onPrev && (
                <button
                  className="fgs__btn fgs__btn--large fgs__btn--primary--outline"
                  onClick={onPrev}
                >
                  {prevText}
                </button>
              )}
              {onNext && (
                <button
                  className="fgs__btn fgs__btn--large bg--secondary-aquamarine"
                  onClick={onNext}
                >
                  {nextText}
                </button>
              )}
            </div>
          )}
        </Modal.Body>
      </Modal>
    );
  }
}

export default SuccessModal;
