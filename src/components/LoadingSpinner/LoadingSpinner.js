import React, { Component } from 'react';
import { PulseLoader } from 'react-spinners';
import './LoadingSpinner.scss';

class LoadingSpinner extends Component {
  render() {
    const { isLoading, size = 8 } = this.props;
    return (
      <div className="loading__spinner__container">
        <PulseLoader
          className="loading__spinner"
          sizeUnit="px"
          size={size}
          color="#01cbc6"
          loading={isLoading}
        />
      </div>
    );
  }
}

export default LoadingSpinner;
