import React, { Component } from 'react';
import uuid from 'uuid';
import { ButtonToolbar, SplitButton, MenuItem } from 'react-bootstrap';
import './Pagination.scss';

class Pagination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: props.sizePerPage,
    };
  }

  onSelect = (eventKey, event) => {
    this.setState({ title: eventKey });
    if (eventKey === 'All') {
      this.props.onChangePerPage(this.props.total);
    } else {
      this.props.onChangePerPage(eventKey);
    }
  };

  renderPaginationButton = (pages) => {
    const width = document.body.clientWidth;
    let paginationSize = 5;
    if (width < 720) {
      paginationSize = 3;
    }

    const { currentPage } = this.props;
    const listBtn = [];
    let start = currentPage;
    if (pages > paginationSize) {
      if (pages - currentPage + 1 < paginationSize) {
        start = pages - paginationSize + 1;
      }
    } else {
      start = 1;
    }

    for (let i = start; i < start + paginationSize; i += 1) {
      if (i <= pages) {
        const btn = (
          <button
            key={uuid.v4()}
            onClick={() => this.props.onChangePage(i)}
            className={this.props.currentPage === i ? 'active' : ''}
          >
            {i}
          </button>
        );
        listBtn.push(btn);
      }
    }
    const res = listBtn.map(item => item);
    return res;
  };

  render() {
    const {
      total, sizePerPage,
      start, to, currentPage,
      showText = true,
    } = this.props;
    const { title } = this.state;

    const listSizes = [10, 20, 50];
    const pages = Math.ceil(total / sizePerPage);
    return (
      <div className="fgs__pagination">
        {
          showText ? <p>Showing {start} to {to < total ? to : total} of {total} entries</p> : null
        }

        <div className="fgs__pagination__buttons">
          <ButtonToolbar>
            <SplitButton title={title} dropup id="dropdown-pagination">
              {listSizes.map(item => (
                <MenuItem key={item} eventKey={item} onSelect={this.onSelect}>
                  {item}
                </MenuItem>
              ))}
              <MenuItem key="All" eventKey="All" onSelect={this.onSelect}>
                All
              </MenuItem>
            </SplitButton>
          </ButtonToolbar>

          <div className="fgs__pagination__right d__flex">
            <button onClick={() => this.props.onChangePage(1)}>
              <i className="icon-first-page" />
            </button>
            <button
              onClick={() => this.props.onChangePage(currentPage - 1)}
              disabled={currentPage === 1}
            >
              <i className="icon-chevron-left" />
            </button>
            {this.renderPaginationButton(pages)}
            <button
              onClick={() => this.props.onChangePage(currentPage + 1)}
              disabled={currentPage === pages}
            >
              <i className="icon-chevron-right" />
            </button>
            <button onClick={() => this.props.onChangePage(pages)}>
              <i className="icon-last-page" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Pagination;
