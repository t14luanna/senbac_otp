import React, { Component } from 'react';
import './Card.scss';

class Card extends Component {
  render() {
    const {
      active, data, type,
    } = this.props;
    return (
      <div
        className={`fgs__card ${active ? 'fgs__card--active' : ''}`}
        onClick={this.props.handleChangeActiveLabel}
      >
        <div className="fgs__card__item">
          <h1>{data.total}</h1>
          <h4 className="trending trending--up">+ {data.newUsers}</h4>
        </div>
        <div className="fgs__card__item">
          <h4>{type}</h4>
          {
            data.percentage > 0 &&
            <h4 className={`trending trending--${data.upTrend ? 'down' : 'up'}`}>
              <i className={`icon-trending-${data.upTrend ? 'down' : 'up'}`} />
              <span>{data.percentage}%</span>
            </h4>
          }
        </div>
      </div>
    );
  }
}

export default Card;
