import React, { Component } from 'react';

import './AuthenticationCard.scss';

export default class AuthenticationCard extends Component {
  render() {
    return (
      <React.Fragment >
        <div className="authentication__card" style={this.props.styling}>
          <div className="authentication__card__title_wrapper">
            <p className="authentication__card__title">{this.props.title}</p>
          </div>
          {this.props.children}
        </div>
      </React.Fragment>
    );
  }
}
