import React, { Component } from 'react';
import './Checkbox.scss';

class Checkbox extends Component {
  render() {
    const {
      name, checked, onChange, label,
    } = this.props;

    return (
      <label className="checkbox__container">
        <input
          name={name}
          type="checkbox"
          checked={checked}
          onChange={onChange}
        />
        {label}
        <span className="checkmark" />
      </label>
    );
  }
}

export default Checkbox;
