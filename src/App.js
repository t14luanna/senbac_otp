import React, { Component } from 'react';

class App extends Component {
  handleSidebarClose = (e) => {
    document.getElementById('mySidebar').classList.remove('is-open');
    document.getElementById('overlay').classList.remove('is-open');
  };

  render() {
    return (
      <div id="wrapper">
        {this.props.children}
        <div id="overlay" onClick={this.handleSidebarClose} />
      </div>
    );
  }
}

export default App;
