import { API } from '../../helpers';
import config from '../../config';

const USER = 'user';
const TOKEN = 'token';

export function login(username, password) {
  return new Promise((resolve, reject) => {
    API.post(config.routes.login, {
      username,
      plainPassword: password,
      language: 'vi',
    }).then((resp) => {
        const { data } = resp;
        console.log(resp.headers['set-cookie']);
        if (data.success) {
          setProfile(data.data);
          return resolve();
        }
        return reject(data);
      })
      .catch(reject);
  });
}

export async function loggedIn() {
  return new Promise((resolve, reject) => {
    return resolve();
    // API.post(config.routes.checkToken, {}, { withCredentials: true })
    // .then(({ data }) => {
    //     if (data.success) {
    //       return resolve();
    //     }
    //     return reject(data);
    //   })
    //   .catch(reject);
  });
}

export function setProfile(user) {
  localStorage.setItem(USER, JSON.stringify(user));
}

export function logout() {
  return new Promise((resolve, reject) => {
    API.post(config.routes.signout).then(({ data }) => {
        if (data.success) {
          return resolve();
        }
        return reject(data);
      })
      .catch(reject);
  });
}

export function getProfile() {
  try {
    const user = JSON.parse(localStorage.getItem(USER));
    if (user) {
      user.displayRole = 'Admin';
      user.userType = 'Admin';
      return user;
    }
    return undefined;
  } catch (error) {
    console.error('getProfile', error.message);
    return undefined;
  }
}
